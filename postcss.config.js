const svelteExtractor = (content) => [
  ...(content.match(/[^<>"'`\s]*[^<>"'`\s:]/g) || []),
  ...(content.match(/(?<=class:)[^=>\/\s]*/g) || []),
];
const purgecss = require("@fullhuman/postcss-purgecss")({
  content: ["./src/**/*.svelte", "./src/**/*.html", "./public/**/*.html"],
  whitelistPatterns: [/svelte-/],
  defaultExtractor: svelteExtractor || [],
});

module.exports = {
  plugins: [...(!process.env.ROLLUP_WATCH ? [purgecss] : [])],
};
