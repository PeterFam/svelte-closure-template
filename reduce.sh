#!/usr/bin/env bash
rm -r public/build/webfonts
cp -r ./node_modules/@fortawesome/fontawesome-free/webfonts/ public/build/webfonts
rm public/build/webfonts/*.eot
echo "#!/usr/bin/fontforge" > reduce.pe
echo 'i=1' >> reduce.pe
echo 'while(i< $argc)' >> reduce.pe
echo 'file= $argv[i]' >> reduce.pe
echo "Open(file)" >> reduce.pe
grep -oE "\.fa-[^[:space:]{})(.,:]+" public/build/bundle.css|sort| uniq|cut -c 5-|awk '{print "SelectMoreIf(\"" $1 "\")"}' >> reduce.pe
echo "SelectInvert()" >>reduce.pe
echo "Clear()" >> reduce.pe
echo "Generate(file)" >> reduce.pe
echo "i++" >> reduce.pe
echo "endloop" >> reduce.pe

chmod +x reduce.pe
./reduce.pe public/build/webfonts/*
